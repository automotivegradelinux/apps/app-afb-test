# LuaUnit Assertion Functions

Follow this section to know which luaUnit assertion functions are available !!

Section's Table of content :

* [General Assertions](0_GeneralAssertions.html)
* [Value Assertions](1_ValueAssertions.html)
* [Scientific Assertions](2_ScientificAssertions.html)
* [String Assertions](3_StringAssertions.html)
* [Error Assertions](4_ErrorAssertions.html)
* [Type Assertions](5_TypeAssertions.html)
* [Table Assertions](6_TableAssertions.html)
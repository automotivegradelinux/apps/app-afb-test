# Installation

[Verify Your Build Host](../../../devguides/reference/1-verify-build-host.html).

Use the following command-line to get the `afb-test` binding and all its
dependencies.

* Fedora (>= 27):

```bash
dnf install agl-app-afb-test-devel
```

* OpenSuse (>= 15.0):

```bash
zypper install agl-app-afb-test-devel
```

* Ubuntu (>= Xenial), Debian stable:

```bash
apt-get install agl-app-afb-test-dev
```
